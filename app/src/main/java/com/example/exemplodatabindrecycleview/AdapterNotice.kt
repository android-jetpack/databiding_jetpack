package com.example.exemplodatabindrecycleview

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.exemplodatabindrecycleview.databinding.AdapterListNoticeBinding
import android.graphics.Movie
import android.util.Log
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions


class AdapterNotice (val list: MutableList<Notice>): RecyclerView.Adapter<AdapterNotice.MyViewHolder>() {


    private var context: Context? = null
    private var pos: Int = -1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        context = parent.context
        val inflate = LayoutInflater.from(parent.context)
        val binding = AdapterListNoticeBinding.inflate(inflate, parent, false)
        return MyViewHolder(binding)
    }


    override fun onBindViewHolder(viewHolder: MyViewHolder, position: Int) {
        val notice = list[position]
        viewHolder.bind(notice)
    }


    override fun getItemCount(): Int = list.size



    inner class MyViewHolder(var binding: AdapterListNoticeBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(notice: Notice) {
            binding.notice = notice
            binding.executePendingBindings()

            if (pos == adapterPosition){
                binding.layoutCell.setBackgroundColor(Color.GRAY)
            }else{
                binding.layoutCell.setBackgroundColor(Color.TRANSPARENT)
            }

            binding.layoutCell.setOnClickListener {
                Log.i("CLICK",binding.notice?.name!!)
                binding.layoutCell.setBackgroundColor(Color.GRAY)

                if (pos == adapterPosition){
                    binding.layoutCell.setBackgroundColor(Color.TRANSPARENT)
                }else{
                    binding.layoutCell.setBackgroundColor(Color.GRAY)
                    pos = adapterPosition
                }
                notifyDataSetChanged()
            }
        }
    }
}
