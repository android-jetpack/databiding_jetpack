package com.example.exemplodatabindrecycleview

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.exemplodatabindrecycleview.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    private var binding: ActivityMainBinding? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setupRecyclerView()
    }




    private fun setupRecyclerView() {
        val list = mutableListOf<Notice>()
        for (i in 1..10){
            list.add(Notice("Noticia $i","descrição $i", "https://lh6.ggpht.com/9SZhHdv4URtBzRmXpnWxZcYhkgTQurFuuQ8OR7WZ3R7fyTmha77dYkVvcuqMu3DLvMQ=w300"))
        }

        binding?.recycleNotice.apply {
            this?.layoutManager = LinearLayoutManager(this@MainActivity)
            this?.adapter = AdapterNotice(list)
        }


        swipeLayout.setOnRefreshListener {
            swipeLayout.isRefreshing = false
            setupRecyclerView()
        }
    }
}
