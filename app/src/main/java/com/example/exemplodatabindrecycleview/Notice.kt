package com.example.exemplodatabindrecycleview

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class Notice(var name: String? = null, var description: String? = null, var imageUrl: String? = null) {

    companion object {
        @JvmStatic
        @BindingAdapter("app:image_url")
        fun setImageUrl(imgView: ImageView, imgUrl: String?) {
            imgUrl?.let {
                Glide.with(imgView.context).load(imgUrl).apply(
                    RequestOptions()
                        .placeholder(R.color.colorPrimary)
                        .error(R.color.colorPrimary)).into(imgView)
            }
        }
    }
}